<?php namespace Gayatri\Insurances\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGayatriInsurances extends Migration
{
    public function up()
    {
        Schema::create('gayatri_insurances_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('gayatri_insurances_');
    }
}
