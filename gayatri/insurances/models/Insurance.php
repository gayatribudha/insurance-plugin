<?php namespace Gayatri\Insurances\Models;

use Model;

/**
 * Model
 */
class Insurance extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'gayatri_insurances_';

    public $attachOne = [
        'bg_img' => 'System\Models\File',
        'proposal_form' => 'System\Models\File',
        'claim_form' => 'System\Models\File',
        'policy_wording' => 'System\Models\File',
        'brochure' => 'System\Models\File'
    ];

    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
